﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.DataExchange;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class NewCodesListeningService : BackgroundService
    {
        private readonly RabbitMqOptions _rabbitMqOptions;
        private readonly IServiceProvider _serviceProvider;

        public NewCodesListeningService(IServiceProvider serviceProvider, IOptionsMonitor<RabbitMqOptions> rabbitMqOptions)
        {
            _serviceProvider = serviceProvider;
            _rabbitMqOptions = rabbitMqOptions.CurrentValue;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(() => Do(stoppingToken), stoppingToken);
        }

        private void Do(CancellationToken stoppingToken)
        {
            using var connection = _rabbitMqOptions.GetConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare("rfp.promo.updatePartner", false, false, false, null);
            var consumer = new EventingBasicConsumer(channel);

            while (!stoppingToken.IsCancellationRequested)
            {
                consumer.Received += async (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var employeeId = FromByteArray(body);
                    await UpdatePartnerAsync(employeeId);
                };

                channel.BasicConsume("rfp.promo.updatePartner", true, consumer);
            }
        }

        private async Task UpdatePartnerAsync(Guid? employeeId)
        {
            if (employeeId == null)
                throw new ArgumentNullException(nameof(employeeId));
            var scope = _serviceProvider.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();
            var employee = await repo.GetByIdAsync(employeeId.Value);

            if (employee == null)
                throw new ApplicationException("Employee is not exists.");

            employee.AppliedPromocodesCount++;

            await repo.UpdateAsync(employee);
        }


        private static Guid? FromByteArray(byte[] data)
        {
            if (data == null)
                return null;
            var bf = new BinaryFormatter();
            using var ms = new MemoryStream(data);
            var obj = bf.Deserialize(ms);
            return (Guid?) obj;
        }
    }
}