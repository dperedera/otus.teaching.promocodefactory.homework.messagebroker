﻿using System;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        
        public void NotifyAdminAboutPartnerManagerPromoCode(IConnection connection, Guid partnerManagerId)
        {
            SendByRabbit(connection, partnerManagerId);
        }
        private static void SendByRabbit(IConnection connection, Guid partnerManagerId)
        {
            var body = partnerManagerId.ToByteArray();
            using var channel = connection.CreateModel();
                channel.QueueDeclare("rfp.promo.updatePartner", false, false, false, null);

                channel.BasicPublish(exchange: "",
                    routingKey: "rfp.promo.updatePartner",
                    basicProperties: null,
                    body: body);
        }
        
    }
}