﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway : IGivingPromoCodeToCustomerGateway
    {
        public void GivePromoCodeToCustomer(IConnection connection, PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            SendByRabbit(connection, dto);
        }

        private static void SendByRabbit(IConnection connection, GivePromoCodeToCustomerDto dto)
        {
         
            using var channel = connection.CreateModel();
            var body = ToByteArray(dto);

            channel.QueueDeclare("rfp.promo.newCode", false, false, false, null);

            channel.BasicPublish(exchange: "",
                routingKey: "rfp.promo.newCode",
                basicProperties: null,
                body: body);
        }

        private static byte[] ToByteArray(GivePromoCodeToCustomerDto obj)
        {
            if (obj == null)
                return null;
            var bf = new BinaryFormatter();
            using var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }
}