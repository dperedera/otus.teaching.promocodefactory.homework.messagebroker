﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IAdministrationGateway
    {
        void NotifyAdminAboutPartnerManagerPromoCode(IConnection connection, Guid partnerManagerId);
    }
}