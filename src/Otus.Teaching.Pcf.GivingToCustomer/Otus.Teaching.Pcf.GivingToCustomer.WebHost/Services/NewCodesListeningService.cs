﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.DataExchange;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class NewCodesListeningService : BackgroundService
    {
        private readonly RabbitMqOptions _rabbitMqOptions;
        private readonly IServiceProvider _serviceProvider;

        public NewCodesListeningService(IServiceProvider serviceProvider,
            IOptionsMonitor<RabbitMqOptions> rabbitMqOptions)
        {
            _serviceProvider = serviceProvider;
            _rabbitMqOptions = rabbitMqOptions.CurrentValue;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(() => Do(stoppingToken), stoppingToken);
        }

        private void Do(CancellationToken stoppingToken)
        {
            using var connection = _rabbitMqOptions.GetConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare("rfp.promo.newCode", false, false, false, null);
            var consumer = new EventingBasicConsumer(channel);

            while (!stoppingToken.IsCancellationRequested)
            {
                consumer.Received += async (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var promoCode = FromByteArray(body);
                    await HandlePromoCodeAsync(promoCode);
                };

                channel.BasicConsume("rfp.promo.newCode", true, consumer);
            }
        }

        private async Task HandlePromoCodeAsync(GivePromoCodeToCustomerDto newCode)
        {
            if (newCode == null)
                throw new ArgumentNullException(nameof(newCode));
            var scope = _serviceProvider.CreateScope();
            var preferenceRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
            var customersRepository = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();
            var promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>();

            var preference = await preferenceRepository.GetByIdAsync(newCode.PreferenceId);
            if (preference == null) 
                throw new ApplicationException("Employee is not preference.");

            var customers = await customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));
            var promoCode = PromoCodeMapper.MapFromModel(newCode, preference, customers);

            await promoCodesRepository.AddAsync(promoCode);
        }


        private static GivePromoCodeToCustomerDto FromByteArray(byte[] data)
        {
            if (data == null)
                return null;
            var bf = new BinaryFormatter();
            using var ms = new MemoryStream(data);
            var obj = bf.Deserialize(ms);
            return (GivePromoCodeToCustomerDto) obj;
        }
    }
}