﻿using System;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.DataExchange
{
    public class RabbitMqOptions
    {
        public string Uri { get; set; }

        public IConnection GetConnection()
        {
            var factory = new ConnectionFactory {Uri = new Uri(Uri)};
            return factory.CreateConnection();
        }
    }
}