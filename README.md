# Otus.Teaching.PromoCodeFactory

Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений.

Подробное описание проекта и описание домашних заданий можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)

Данный проект является стартовой точкой для Homework №7

Описание домашнего задания в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-7)


# Принцип работы (усеченно для ДЗ)

**Отправка сообщений в 2 очереди:**

Otus.Teaching.Pcf.ReceivingFromPartner.WebHost PartnersController.cs


**Прием сообщений:**


1. _Otus.Teaching.Pcf.Administration.WebHost NewCodesListenService.cs_

        Получает id работника (employeeId), увеличивает у него количество промокодов.
        Очередь: rfp.promo.updatePartner


2. _Otus.Teaching.Pcf.GivingToCustomer.WebHost_ **пока контроллер PromocodesController.cs**

        Получает промокод и выдает его клиентам с указанным предпочтением.
        Очередь: rfp.promo.newCode

# Критерий оценивания

1. Подключен RabbitMq, синхронный вызов по HTTP в `_administrationGateway` заменен передачей события в `RabbitMq`, в `Otus.Teaching.Pcf.Administration` реализовано обновление количества промокодов у сотрудника через класс-сервис: 7 баллов;
2. Подключен RabbitMq, синхронные вызовы по HTTP в `_administrationGateway` и `_givingPromoCodeToCustomerGateway` заменены  передачей события в `RabbitMq` (тут можно просто отправить одно событие и подписаться на него в двух сервисах), в `Otus.Teaching.Pcf.Administration` реализовано обновление количества промокодов у сотрудника через класс-сервис, а в `Otus.Teaching.Pcf.GivingToCustomer` реализована выдача промокода клиентам через подписку на событие в `Rabbit`, код перенесен в класс-сервис: 10 баллов.
